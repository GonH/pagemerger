# Page merger

Project that given a directory (collection type e.g.: vacations) of directories (subcategories e.g.: day 1, day 2) joins the images in the last directory into a pdf.

The configuration is given by a js file called `constants`. It has the following info:

- `SOURCE_PATH`: the name of the dir that lives in the same level as the code, its the base path (e.g.: `/vacations`)
- `FIRST_INFO` and `SECOND_INFO`: Jsons with the fields `SEP`, `CH` and `SPLIT` used for parsing names in the `parseName` (index.js) function. This format is for my use case specifically
- `DOWNLOAD_TRASH`: An array of strings referencing files to be ignored in each subdirectory

Another type of configuration is in the file `specificUtils.js`. That module exports a function called `parseName` that, given the dir name, returns a new name for the final pdf. For instance if I have the dir `bariloche 2024 vacations` and `buenos aires 2024 suffering` I could make a function that returns first the year, then the city alias, then the activity. A basic example following the previous titles:

```
const parseName = (dirName) => {
  let replacedName = dirName;
  if (dirName.includes("bariloche"))
    replacedName = dirName.replace("bariloche", "brc");
  if (dirName.includes("buenos aires"))
    replacedName = dirName.replace("buenos aires", "bs_as");

  const dirInfo = replacedName.split(" ");

  const { year, city, activity } = dirInfo.reduce((accum, current) => {
    if (Number(current) && !accum.year) return { ...accum, year: current };

    if (["brc", "bs_as"].includes(current))
      return { ...accum, city: current };

    const activity = accum.activity;

    return {
      ...accum,
      activity: activity ? `${activity}_${current}` : current,
    };
  }, { year: "", city: "", activity: "" });

  return `${year}_${city}_${activity}`;
};
```

TODO:

- Create script to validate which chapters are missing
