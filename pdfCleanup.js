const fs = require("fs");
const { SOURCE_PATH } = require("./constants");

const { getDirList, getSubDir } = require("./utils");

const purgePdfs = async (toPurgeName) => {
  const dirList = getDirList(SOURCE_PATH);
  
  for (let i = 0; i < dirList.length; i++) {
    const dir = dirList[i];
    
    if (toPurgeName && dir !== toPurgeName) continue;

    const subDirs = getSubDir(dir);

    for (let j = 0; j < subDirs.length; j++) {
      const basePath = `${SOURCE_PATH}/${dir}/${subDirs[j]}`;
      
      const rawPageList = fs.readdirSync(basePath);

      const pageList = rawPageList.filter((pageName) =>
        pageName.includes(".pdf")
      );

      for (k = 0; k < pageList.length; k++) {
        console.info("To delete", pageList[k]);
        fs.rmSync(`${basePath}/${pageList[k]}`);
      }
    }
  }
};

purgePdfs()
  .then(() => console.info("end"))
  .catch((error) => console.error("error", error));
