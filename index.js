const imgToPDF = require("image-to-pdf");
const { imageSize } = require("image-size");
const fs = require("fs/promises");
const fsSync = require("fs");
const { getDirList, getSubDir } = require("./utils");
const { SOURCE_PATH, DOWNLOAD_TRASH } = require("./constants");
const { parseName } = require("./specificUtils");

const getSize = (images) => {
  const sizesCount = images.reduce((accum, image) => {
    const { height, width } = imageSize(image);
    const key = `${height}@${width}`;
    const count = accum[key] || 0;

    return { ...accum, [key]: count + 1 };
  }, {});

  const dimensionKey = Object.keys(sizesCount).reduce(
    (maxKey, key) =>
      !maxKey || sizesCount[key] > sizesCount[maxKey] ? key : maxKey,
    ""
  );

  const [height, width] = dimensionKey.split("@");

  return { height: Number(height), width: Number(width) };
};

const tranformDir = async (dirName, subdirName) => {
  const basePath = `${SOURCE_PATH}/${dirName}/${subdirName}`;

  const rawPageList = await fs.readdir(basePath);
  const pageList = rawPageList.filter(
    (pageName) => !DOWNLOAD_TRASH.includes(pageName)
  );

  if (pageList.some((page) => !page.includes("jpg") && !page.includes("png")))
    return;

  const parsedPages = pageList.map((pageName) => `${basePath}/${pageName}`);

  const pdfName = `${basePath}/${dirName}_${parseName(dirName, subdirName)}.pdf`;

  try {
    const { height, width } = getSize(parsedPages);

    const pipe = imgToPDF(parsedPages, [width, height]).pipe(
      fsSync.createWriteStream(pdfName)
    );

    return new Promise((resolve) => {
      pipe.on("close", () => {
        resolve(pdfName);
      });
    });
  } catch (error) {
    console.error(
      "Error",
      error,
      "at dir",
      dirName,
      "and dir name",
      subdirName,
      `(extra info: ${parsedPages[0]})`
    );
  }
};

const executeDirTransformation = async () => {
  const dirList = getDirList(SOURCE_PATH);
  const destinationDirList = getDirList("./pdfs");
  let wrote = false;

  for (let i = 0; i < dirList.length; i++) {
    const dir = dirList[i];

    if (!destinationDirList.includes(dir))
      await fs.mkdir(`./pdfs/${dir}`);

    const subDirs = getSubDir(dir);

    for (let j = 0; j < subDirs.length; j++) {
      const subdirName = subDirs[j];

      const pdfPath = await tranformDir(dir, subdirName);

      if (pdfPath) {
        const bySlash = pdfPath.split("/");
        const fileName2 = bySlash[bySlash.length - 1];

        await fs.copyFile(pdfPath, `./pdfs/${dir}/${fileName2}`);
        wrote = true;
      }
    }

    if (wrote) {
      console.info("Wrote into", dir);
      wrote = false;
    }
  }
};

executeDirTransformation()
  .then(() => console.info("end"))
  .catch((error) => console.error("error", error));
