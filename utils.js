const fs = require("fs");
const { SOURCE_PATH } = require("./constants");

const getDirList = (path) => fs.readdirSync(path);

const getSubDir = (mangaName) => fs.readdirSync(`${SOURCE_PATH}/${mangaName}`);

module.exports = {
  getDirList,
  getSubDir,
};
